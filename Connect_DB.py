import mariadb
import sys

conn = None
cur = None

host = '127.0.0.1'
user = 'root'
pw = '1014' # 패스워드
db = 'kiosk'

# DB 정보 및 연결
try:
    conn = mariadb.connect (host = host, user = user, password = pw, db = db)
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

cur = conn.cursor() # 커서 생성

# 로그인
def login():
    login_sql = "SELECT id, password FROM user"
    cur.execute(login_sql)

    data = cur.fetchall()

    print("---------로그인---------")
    id1 = str(input("id : ")) # id 입력받기
    password1 = str(input("password : ")) # password 입력받기

    if id1 == 'admin' and password1 == '0000':
        print("관리자 로그인 성공")
        selection()
    else:
        print("아이디 또는 비밀번호가 틀렸습니다.")



# 정보 추가
def insert():
    print("추가할 정보를 입력해주세요 (아이디, 비밀번호, 이름)")
    id2 = str(input("id : "))
    password2 = str(input("password : "))
    name2 = str(input("name : "))
    insert_sql = "INSERT INTO user VALUES (%s, %s, %s)"
    val = (id2, password2, name2)
    cur.execute(insert_sql, val)


    conn.commit()

    print("추가 성공")

# 검색 및 열람
def select():
    print("ID로 검색 : 1 / Password로 검색 : 2 / Name로 검색 : 3 / 정보 열람 : 4")
    n = int(input("번호를 선택해주세요 >> "))
    if n == 1:
        id2 = str(input("검색할 ID를 입력해주세요 >> "))
        select_sql = "SELECT * FROM user WHERE id = '%s'"  # sql 문
        cur.execute(select_sql, id2)
        data = cur.fetchall()
        if data == None:
            print("검색 결과 없음")
        else:
            print(data)


    elif n == 2:
        pw2 = str(input("검색할 Password를 입력해주세요 >> "))
        select_sql = "SELECT * FROM user WHERE password = '%s'"  # sql 문
        val = (pw2)
        cur.execute(select_sql, val)
        data = cur.fetchall()
        if data == None:
            print("검색 결과 없음")
        else:
            print(data)

    elif n == 3:
        name2 = str(input("검색할 Name을 입력해주세요 >> "))
        select_sql = "SELECT * FROM user WHERE name = '%s'"  # sql 문
        val = (name2)
        cur.execute(select_sql, val)
        data = cur.fetchall()
        if data == None:
            print("검색 결과 없음")
        else:
            print(data)
    else:
        select_sql = "SELECT * FROM user "  # sql 문
        cur.execute(select_sql)
        data = cur.fetchall()

        for x in data:
            print(x)




# 수정

def update():
    print("수정할 종류를 선택하세요")
    n = int(input("1. Password  2. Name >> "))
    if n == 1:
        id2 = str(input("수정할 id를 입력해주세요 >> "))
        pw2 = str(input("수정할 Password를 입력해주세요. >> "))
        update_sql = "UPDATE user SET PASSWORD = '%s' WHERE id = '%s'"  # sql 문
        val =(pw2, id2)
        cur.execute(update_sql, val)
        conn.commit()
        data = cur.fetchall()
        print(data)


    else:
        id2 = str(input("수정할 id를 입력해주세요 >> "))
        name2 = str(input("수정할 Name을 입력해주세요. >> "))
        update_sql = "UPDATE user SET NAME = '%s' WHERE id = '%s'"  # sql 문
        val = (name2, id2)
        cur.execute(update_sql, val)
        conn.commit()
        data = cur.fetchall()
        print(data)

# 삭제
def delete():
    print("삭제할 정보의 id를 입력하세요 >> ")
    id2 = str(input("id : "))
    delete_sql = "DELETE FROM user where id = '%s'"  # sql 문
    val = (id2)
    cur.execute(delete_sql, val)

    conn.commit()

    print("삭제되었습니다.")


# 선택
def selection():
    print("추가 : 1 / 열람 및 검색 : 2 / 수정 : 3 / 삭제 : 4")
    n = int(input("번호를 선택하세요 >> "))
    if n == 1:
        insert()
    elif n == 2:
        select()
    elif n == 3:
        update()
    else:
        delete()


login()

conn.close()
