import mysql.connector
from mysql.connector import Error

def convertToBinaryData(filename):
    # Convert digital data to binary format
    with open(filename, 'rb') as file:
        binaryData = file.read()
    return binaryData

def insertBLOB(number, name, photo):
    print("Inserting BLOB into images table")
    try:
        connection = mysql.connector.connect(host='127.0.0.1',
                                             database='kiosk',
                                             user='root',
                                             password='1014')

        cursor = connection.cursor()
        sql_insert_blob_query = """ INSERT INTO images
                          (id, name, photo) VALUES (%s,%s,%s)"""

        Picture = convertToBinaryData(photo)
       # file = convertToBinaryData(biodataFile)

        # Convert data into tuple format
        insert_blob_tuple = (number, name, Picture)
        result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)
        connection.commit()
        print("Image and file inserted successfully as a BLOB into images table", result)

    except mysql.connector.Error as error:
        print("Failed inserting BLOB data into MySQL table {}".format(error))

    finally:
        if (connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")
            print(Picture)

insertBLOB(3, "녹차라떼", "C:/Users/pc/PycharmProjects/Kiosk__/Image/녹차라떼.PNG")
